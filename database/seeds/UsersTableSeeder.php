<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Administrator',
            'username' => 'admin',
            'password' => bcrypt('admin123')
        ]);

        User::create([
            'name' => 'Kasir 1',
            'username' => 'kasir_1',
            'password' => bcrypt('kasir123')
        ]);
    }
}
